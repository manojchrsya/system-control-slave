"use strict;"

var express = require('express');
	sys = require('sys'),
	exec = require('child_process').exec,
	os = require("os"),
	request = require("request");
	fs = require("fs-extra"); 
	async = require("async");
	clientConfig = require("./client.json");
	system = require("./helpers");

var app = express();
var bodyParser = require('body-parser')
app.use( bodyParser.json() ); 
app.use(bodyParser.urlencoded({extended: true}));

if(clientConfig.hasOwnProperty('host')){
	targetUrl = clientConfig.host;
	if(clientConfig.hasOwnProperty('port')){
		targetUrl = configUrl+":"+clientConfig.port;
	}
}else{
	console.log("client side configuration not found!");
	process.exit(1);
}

var configUrl =	targetUrl+"/config.json";
(function sendOSDetails(url) {

	var osDetails = osDetails || {};
	osDetails.name = os.hostname();
	osDetails.details = os.networkInterfaces();
	// send os details to server
	async.waterfall([
		function(callback){
			fs.readFile('./config.json', 'utf8' , function(err, data){
				if(!err){
					callback(null, {data: JSON.parse(data)});
				}else{
					callback(true, err);
				}
			});
		}, function(response, callback){
			osDetails.config = response.data;
			callback(null, osDetails);
		}], function(err, data){
			if(!err){
				request.post({
					url: targetUrl+"/system/details", 
					form: {details:	osDetails}
				}, function callback(err, httpResponse, body) {
			  		if (err) {
			    		return console.error('\nsorry for inconvinience,currently our server is down!.\n', err);
			    		process.exit(1);
			  		}
				});
			}else{
				console.log("oopss! something went wrong \n"+ err);
			}
		});
})(configUrl);

system.copyFile();
system.copyScript();

function getCommands(url){
	request({
	    url: url,
	    headers: {
	    	'X-SYSTEM': os.hostname(), 	
	    	'Content-Type': 'application/json'
	    },
	    json: true
	}, function (error, response, commands) {
	    if (!error && response.statusCode === 200) {
	    		
	    	// itrating the commands from config
	        for(var i in commands){
	        	var  cmd = commands[i];
	        	if(cmd.status == 'true'){ // if status is true then execute that command
	        		switch(cmd.title) {
					    case 'CREATE_DIR':
					        system.createDir(cmd); // creating the directory 
					        break;
					    case 'LOGOFF_SYS':
							system.logOffSystem(cmd); // log off the system and kill all the process
							break;
						case  'SUSPEND_SYS':
							system.suspendSystem(cmd); // suspend system 
							break;
						case 'RESTART_SYS':
							system.restartSystem(cmd); // restart the system
							break;
						case 'SHUTDOWN_SYS':
							system.shutdownSystem(cmd); // shut down the system
							break;								    	    
					    default:
					        console.log("default block!!"); 
					        break;
					}        
	        	}
	        }
	    }
	})
}

// ping the server at perticular time interval
console.log("-------------waiting for changes from server!------------------");
setInterval(function(){
	getCommands(configUrl); 
},1000);


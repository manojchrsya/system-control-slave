"use strict;"

var system = {
	createDir: function(data){
		var folderName = Date.now();
		var cmd = data.command+" "+data.path+""+folderName;
		this.execCommand(cmd, function(err, data){
			if(err){ 
				console.log(err) 
			}else {
				console.log(folderName+" folder created successfully!");	
			} 
		});
	},
	copyFile: function(){
		var path = "~/tmp";
		var _that = this
		_that.execCommand('pwd', function(err, res){
			if(!err) {	
				var source = res.replace("\n", "")+"/"+"app.js";
				var destination = path;
				var cpcmd = "cp "+source+" "+destination;
				_that.execCommand(cpcmd, function(err, data){
					console.log(err);	
				});
			}else{ 
				console.log(err); 
			};
		}); 
	},
	copyScript: function(){
		// copies a file to system for auto-start the script
		fs.copy("myapp.conf","/etc/init/myapp.conf", function (err, data) {
		    if (err){
		    	console.log('Please run the script with administrator privilage!\n'); 
		    	console.log(err); 	
		    	process.exit(1);
		    } 
		});
	},
	getUserName: function(next){
		this.execCommand('whoami', function(err, res){
			if(!err) {	next(res) } else{ console.log(err); };
		});
	},
	logOffSystem: function(data){
		var _that = this
		_that.getUserName(function(res){
			var cmd = data.command+" "+res;
			_that.execCommand(cmd, function(err, data){
				console.log(err);
			});
		});
	},
	suspendSystem: function(data){
		var cmd = data.command;
		this.execCommand(cmd, function(err, data){
			console.log(err);
			//process.exit(1);
		});
	},
	restartSystem: function(data){
		var cmd = data.command;
		this.execCommand(cmd, function(err, data){
			console.log(err);
		});
	},
	shutdownSystem: function(data){
		var cmd = data.command;
		this.execCommand(cmd, function(err, data){
			console.log(err);
		});
	},
	execCommand: function(cmd, callback){
		exec(cmd, function (error, stdout, stderr) {
			//sys.print('stdout: ' + stdout);
			//sys.print('stderr: ' + stderr);
			if (error) {
			    console.log('could not execute the command!\n');
			    console.log(error);
			};
			callback(error, stdout);
		});
	}
};

module.exports = system;